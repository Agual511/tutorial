package gokulist

class UsuarioController {

 	def scaffold = Usuario

 	def login = { 
	}

	def doLogin = {

		try{
			def usuario = Usuario.findWhere(nombreUsuario:params['nombreUsuario'], password:params['password'])
			session.user = usuario
	 		if (usuario){
	 			log.info "Encontre usuario: " + usuario.nombreUsuario
	 			redirect(controller:'lista',action:'index')

	 		}
	 		else{
	 			log.error "Hubo un error"
	 			redirect(controller:'usuario',action:'login')	
	 		}
	 			
		}catch(Exception e){
			log.error "Error buscando usuario" + e.message 
		} 		
		
	}

	
}
