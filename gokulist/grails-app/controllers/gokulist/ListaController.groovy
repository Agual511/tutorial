package gokulist



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import gokulist.NivelUsuario
import gokulist.Todo

@Transactional(readOnly = true)
class ListaController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def servicioNivelesService

    def beforeInterceptor = [action:this.&checkUser,except:['list','show']]

    def checkUser() {

        //println "Hello world. Chequeando usuario"

        if(!session.user) {
            // i.e. user not logged in
            log.info "Entrando en checkUser"
            redirect(controller:'usuario',action:'login')
            return false
        }
    }



    def index(Integer max) {
        //redirect(action: "list", params: params)
        params.max = Math.min(max ?: 10, 100)
        
        def usr = Usuario.findWhere(id:session.user.id)

        //def listas = Lista.findAllByUsuarios(usr)

        //respond Lista.list(params), model:[listaInstanceCount: Lista.count()]
        respond usr.listas.toList(), model:[listaInstanceCount: Lista.count()]
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        //[listaInstanceList: Lista.list(params), listaInstanceTotal: Lista.count()]
        respond Lista.list(params), model:[listaInstanceCount: Lista.count()]
    }

    def show(Lista listaInstance) {

        println "el nombre de la lista en show es ${listaInstance.getNombre()}"
        
        def lista = Lista.findWhere(id:listaInstance.id)

        def todoInstanceList = Todo.findAllByLista(lista)

        

        respond listaInstance, model:[todoInstanceList:todoInstanceList, listaid: listaInstance.id]
    }

    @Transactional
    def create() {

        //Usuario usr = session.user

        def aux = servicioNivelesService.UsuarioTieneAcceso(session.user.nombreUsuario,session.user.password)

        if(aux)
            //render (view:"plan")
            redirect(controller: "PlanDePago", action: "index")
        else{
            //session.user.cantListas++
            //session.user.save(flush: true)
            servicioNivelesService.IncrementarListaEnUsuario(session.user.nombreUsuario,session.user.password)
            respond new Lista(params)
        }

        //println session.user.fechaSuscripcion
        
        //session.user.cantListas = cantListas + 1
        //session.user.save(flush:true) 
    }

    def auxiliar(){
        
    }

    @Transactional
    def save(Lista listaInstance) {
        if (listaInstance == null) {
            notFound()
            return
        }

        if (listaInstance.hasErrors()) {
            respond listaInstance.errors, view:'create'
            return
        }



        listaInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'lista.label', default: 'Lista'), listaInstance.id])
                redirect listaInstance
            }
            '*' { respond listaInstance, [status: CREATED] }
        }
    }

    @Transactional
    def saveTodo(Todo todoInstance) {
        if (todoInstance == null) {
            notFound()
            return
        }

        if (todoInstance.hasErrors()) {


                todoInstance.errors.each {
                    println it
                }
            
            respond todoInstance.errors, view:'create'
            return
        }


        todoInstance.save flush:true

        //request.withFormat {
        //    form multipartForm {
        //        flash.message = message(code: 'default.created.message', args: [message(code: 'todo.label', default: 'Todo'), todoInstance.id])
        //        redirect todoInstance
        //    }
        //    '*' { respond todoInstance, [status: CREATED] }
        //}

        
        

        //def idLista = Todo.executeQuery("select t.listaId from Todo t where t.id = ${todoInstance.id}")
        def idLista = todoInstance.devolverIdLista()
        println "el id es ${idLista}"
        def lista = Lista.findWhere(id:idLista)

        //println "el nombre de la lista es ${lista.getNombre()}"

        //redirect(action: "show", model:[listaInstance: lista.id])
        redirect(action: "show",id:idLista)
        //chain(action:'show',model:[listaInstance:lista])

        //forward action: "show", id: 4, params: [author: "Stephen King"]

        //show(lista)
    }

    def edit(Lista listaInstance) {
        respond listaInstance
    }

    @Transactional
    def update(Lista listaInstance) {
        if (listaInstance == null) {
            notFound()
            return
        }

        if (listaInstance.hasErrors()) {
            respond listaInstance.errors, view:'edit'
            return
        }

        listaInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Lista.label', default: 'Lista'), listaInstance.id])
                redirect listaInstance
            }
            '*'{ respond listaInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Lista listaInstance) {

        if (listaInstance == null) {
            notFound()
            return
        }

        listaInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Lista.label', default: 'Lista'), listaInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'lista.label', default: 'Lista'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
