package gokulist

import gokulist.NivelUsuario

class PlanDePagoController {

	def servicioNivelesService

    def index() { 
    }


    def pasarBronze(){
    	servicioNivelesService.PasarUsuarioANivel(session.user.nombreUsuario,session.user.password, NivelUsuario.BRONZE)
    	redirect(controller:'lista',action:'create')
    }

    def pasarSilver(){
    	servicioNivelesService.PasarUsuarioANivel(session.user.nombreUsuario,session.user.password, NivelUsuario.SILVER)
    	redirect(controller:'lista',action:'create')
    }

    def pasarGoku(){
    	servicioNivelesService.PasarUsuarioANivel(session.user.nombreUsuario,session.user.password, NivelUsuario.GOKU)
    	redirect(controller:'lista',action:'create')
    }

    def seguirFree(){
    	redirect(controller:'lista',action:'index')	
    }
}
