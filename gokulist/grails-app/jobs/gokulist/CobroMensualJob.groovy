package gokulist



class CobroMensualJob {

	def servicioNivelesService

    static triggers = {

      //simple repeatInterval: 5000l // execute job once in 5 seconds

      cron name: 'myTrigger', cronExpression: "0 17 20 * * ?"

    }



    def execute() {

        Date now = new Date()

		print 'Entra'        

        for(Usuario u in Usuario.getAll()) {

        	//print now

        	def mesDistinto = u.fechaSuscripcion.month != now.month
        	def anoDistinto = u.fechaSuscripcion.year != now.year
        	def mismoDia = u.fechaSuscripcion.date == now.date

        	//print mesDistinto
        	//print anoDistinto
        	//print mismoDia

        	if (mismoDia && !(mesDistinto && anoDistinto)) {

        		if(u.nivelUsuario == NivelUsuario.GOKU){
        			print 'cobrar 20'
        		}

        		else if(u.nivelUsuario == NivelUsuario.SILVER){
        			print 'cobrar 10'
        		}

        		else if(u.nivelUsuario == NivelUsuario.BRONZE){
        			print 'cobrar 5'
          		}        		

          		servicioNivelesService.PasarUsuarioANivelPorCantidadDeListas(u.nombreUsuario,u.password)
        	}
        }
    }
}
