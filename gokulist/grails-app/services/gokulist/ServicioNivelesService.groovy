package gokulist

import grails.transaction.Transactional

@Transactional
class ServicioNivelesService {

    def UsuarioTieneAcceso(nombreUsuario, password){

    	def usuario = Usuario.findWhere(nombreUsuario:nombreUsuario,password:password)


        def cantListas = usuario.cantListas
        def nivelUsuario =usuario.nivelUsuario

        //println cantListas
        //println NivelUsuario.FREE

        def aux = false

        if(cantListas == 5 && nivelUsuario == NivelUsuario.FREE){
            println "Usuario llego a 5 listas y es free"
            aux = true
        }
        if(cantListas == 15 && nivelUsuario == NivelUsuario.BRONZE){
            println "Usuario llego a 15 listas y es bronce"
            aux = true
        }
        if(cantListas == 40 && nivelUsuario == NivelUsuario.SILVER){
            println "Usuario llego a 40 listas y es silver"
            aux = true
        }

        return aux
    }

    def IncrementarListaEnUsuario(nombreUsuario, password){

        def usuario = Usuario.findWhere(nombreUsuario:nombreUsuario,password:password)        

        usuario.cantListas++
        usuario.save(flush: true)
    }

    def PasarUsuarioANivel(nombreUsuario,password,nivelUsuario){

        def usuario = Usuario.findWhere(nombreUsuario:nombreUsuario,password:password)

        if(nivelUsuario == NivelUsuario.BRONZE){
            usuario.nivelUsuario = NivelUsuario.BRONZE
        }
        if(nivelUsuario == NivelUsuario.SILVER){
            usuario.nivelUsuario = NivelUsuario.SILVER
        }
        if(nivelUsuario == NivelUsuario.GOKU){
            usuario.nivelUsuario = NivelUsuario.GOKU 
        }

        Date now = new Date()
        usuario.fechaSuscripcion = now
        usuario.save(flush: true)
    }

    def PasarUsuarioANivelPorCantidadDeListas(nombreUsuario,password){

        def usuario = Usuario.findWhere(nombreUsuario:nombreUsuario,password:password)
        def cantListas = usuario.cantListas

        if(cantListas < 6 && (usuario.nivelUsuario != NivelUsuario.FREE)){
            usuario.nivelUsuario = NivelUsuario.FREE
            TestMail(usuario)
        }
        if(cantListas > 5 && cantListas < 16 && usuario.nivelUsuario != NivelUsuario.BRONZE){
            usuario.nivelUsuario = NivelUsuario.BRONZE
            TestMail(usuario)
        }
        if(cantListas > 15 && cantListas < 41 && usuario.nivelUsuario != NivelUsuario.SILVER){
            usuario.nivelUsuario = NivelUsuario.SILVER
            TestMail(usuario)
        }
        usuario.save(flush: true)
    }

    def TestMail(usuario) {
        sendMail {
            to usuario.email
            subject "Gokulist cuida tu dinero!"
            body 'Debido a la cantidad de listas que posees te hemos bajado de nivel para que no malgastes tu dinero. Pero tranquilo, podras volver a subir de nivel si vuelves a crear mas listas.'
        }
    }
}
