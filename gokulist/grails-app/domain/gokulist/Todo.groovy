package gokulist

class Todo {

	String contenido
	Boolean completada
	Date dateCreated

	Lista lista

	def devolverIdLista(){
		lista.id
	}

	static belongsTo = [lista: Lista]

    static constraints = {
    }
}
