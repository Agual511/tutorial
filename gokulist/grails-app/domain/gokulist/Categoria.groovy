package gokulist

class Categoria {

	static hasMany = [lista: Lista]
	String nombre

    static constraints = {
    }

    String toString(){ 
    	"$nombre" 
    }
}
