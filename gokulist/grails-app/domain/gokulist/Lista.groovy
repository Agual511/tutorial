package gokulist

class Lista {

	String nombre

	static hasOne = [categoria: Categoria]
	static hasMany = [usuarios: Usuario]
	static belongsTo = Usuario

    static constraints = {
    }

    String toString(){ 
    	"$nombre" 
    }
}
