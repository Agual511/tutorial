package gokulist

import org.grails.databinding.BindingFormat

class Usuario {

	String nombre
	String nombreUsuario
	String password
	String email
    Integer cantListas
    NivelUsuario nivelUsuario

    @BindingFormat('yyyy-MM-dd  HH:mm')
    Date fechaSuscripcion

	static hasMany = [listas: Lista]

    static constraints = {
    	nombre blank: false
    	nombreUsuario size:5..15, blank:false, unique:true
    	password size: 5..15, blank:false
    	email email:true, blank: false
        
    }

    String toString(){ 
        "$nombreUsuario" 
    }
}
