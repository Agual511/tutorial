<%@ page import="gokulist.Todo" %>



<div class="fieldcontain ${hasErrors(bean: todoInstance, field: 'completada', 'error')} ">
	<label for="completada">
		<g:message code="todo.completada.label" default="Completada" />
		
	</label>
	<g:checkBox name="completada" value="${todoInstance?.completada}" />

</div>

<div class="fieldcontain ${hasErrors(bean: todoInstance, field: 'contenido', 'error')} required">
	<label for="contenido">
		<g:message code="todo.contenido.label" default="Contenido" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="contenido" required="" value="${todoInstance?.contenido}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: todoInstance, field: 'lista', 'error')} required">
	<label for="lista">
		<g:message code="todo.lista.label" default="Lista" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="lista" name="lista.id" from="${gokulist.Lista.list()}" optionKey="id" required="" value="${todoInstance?.lista?.id}" class="many-to-one"/>

</div>

