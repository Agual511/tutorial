
<%@ page import="gokulist.Todo" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'todo.label', default: 'Todo')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-todo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-todo" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list todo">
			
				<g:if test="${todoInstance?.completada}">
				<li class="fieldcontain">
					<span id="completada-label" class="property-label"><g:message code="todo.completada.label" default="Completada" /></span>
					
						<span class="property-value" aria-labelledby="completada-label"><g:formatBoolean boolean="${todoInstance?.completada}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${todoInstance?.contenido}">
				<li class="fieldcontain">
					<span id="contenido-label" class="property-label"><g:message code="todo.contenido.label" default="Contenido" /></span>
					
						<span class="property-value" aria-labelledby="contenido-label"><g:fieldValue bean="${todoInstance}" field="contenido"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${todoInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="todo.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${todoInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${todoInstance?.lista}">
				<li class="fieldcontain">
					<span id="lista-label" class="property-label"><g:message code="todo.lista.label" default="Lista" /></span>
					
						<span class="property-value" aria-labelledby="lista-label"><g:link controller="lista" action="show" id="${todoInstance?.lista?.id}">${todoInstance?.lista?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:todoInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${todoInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
