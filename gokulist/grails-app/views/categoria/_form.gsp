<%@ page import="gokulist.Categoria" %>



<div class="fieldcontain ${hasErrors(bean: categoriaInstance, field: 'lista', 'error')} ">
	<label for="lista">
		<g:message code="categoria.lista.label" default="Lista" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${categoriaInstance?.lista?}" var="l">
    <li><g:link controller="lista" action="show" id="${l.id}">${l?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="lista" action="create" params="['categoria.id': categoriaInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'lista.label', default: 'Lista')])}</g:link>
</li>
</ul>


</div>

<div class="fieldcontain ${hasErrors(bean: categoriaInstance, field: 'nombre', 'error')} required">
	<label for="nombre">
		<g:message code="categoria.nombre.label" default="Nombre" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nombre" required="" value="${categoriaInstance?.nombre}"/>

</div>

