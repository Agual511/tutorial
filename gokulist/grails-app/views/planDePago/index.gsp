
<%@ page import="gokulist.Lista" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'lista.label', default: 'Lista')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<h1 style="padding:10px">Elija el plan que desea:</h1>

<hr />
<p>&nbsp;</p>

<p style="padding:10px"><ins><strong>Bronze</strong></ins></p> 

<p>&nbsp;</p>

<div style="background:#eee; border:1px solid #ccc; padding:5px 10px">Este plan le permite tener hasta 15 listas a su disposici&oacute;n</div>

<p>&nbsp;</p>

<g:link action="pasarBronze" class="btnSelect" id="${BRONZE}">Seleccionar</g:link>

<p>&nbsp;</p>

<hr />
<p>&nbsp;</p>

<p style="padding:10px"><strong><ins>Silver</ins></strong></p>

<p><strong>&nbsp;</strong></p>

<div style="background:#eee; border:1px solid #ccc; padding:5px 10px">Este plan le permite tener hasta 40 listas a su disposici&oacute;n.</div>

<p>&nbsp;</p>

<g:link action="pasarSilver" class="btnSelect" id="${SILVER}">Seleccionar</g:link>

<p>&nbsp;</p>

<hr />
<p>&nbsp;</p>

<p style="padding:10px"><ins><strong>Goku</strong></ins></p>

<p>&nbsp;</p>

<div style="background:#eee; border:1px solid #ccc; padding:5px 10px">Este plan le permite tener todas las listas que usted desee.</div>

<p>&nbsp;</p>

<g:link action="pasarGoku" class="btnSelect" id="${GOKU}">Seleccionar</g:link>

<p>&nbsp;</p>
<hr />
<p>&nbsp;</p>

<p style="padding:10px"><strong><ins>Free</ins></strong></p>

<p>&nbsp;</p>

<div style="background:#eee; border:1px solid #ccc; padding:5px 10px">Este plan le permite tener hasta 5 listas a su disposici&oacute;n.</div>

<p>&nbsp;</p>

<g:link action="seguirFree" class="btnSelect" >Seleccionar</g:link>

<p>&nbsp;</p>

<hr />
<p>&nbsp;</p>
	</body>
</html>
