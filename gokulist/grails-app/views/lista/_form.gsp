<%@ page import="gokulist.Lista" %>



<div class="fieldcontain ${hasErrors(bean: listaInstance, field: 'categoria', 'error')} required">
	<label for="categoria">
		<g:message code="lista.categoria.label" default="Categoria" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="categoria" name="categoria.id" from="${gokulist.Categoria.list()}" optionKey="id" required="" value="${listaInstance?.categoria?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: listaInstance, field: 'nombre', 'error')} required">
	<label for="nombre">
		<g:message code="lista.nombre.label" default="Nombre" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nombre" required="" value="${listaInstance?.nombre}"/>

</div>

<!--<div class="fieldcontain ${hasErrors(bean: listaInstance, field: 'usuarios', 'error')} ">
	<label for="usuarios">
		<g:message code="lista.usuarios.label" default="Usuarios" />
		
	</label>
	

</div>-->

