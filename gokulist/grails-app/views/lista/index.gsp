
<%@ page import="gokulist.Lista" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'lista.label', default: 'Lista')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-lista" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<!--<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>-->
				<!--<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>-->
				<li><g:link class="create" controller="lista" action="create"><g:message message="Nueva Lista" /></g:link></li>
			</ul>
		</div>
		
			<div id="list-lista" class="first" role="main">

				<h1><g:message code="default.list.label" args="[entityName]" /></h1>
				<g:if test="${flash.message}">
					<div class="message" role="status">${flash.message}</div>
				</g:if>
				<table>
					<thead>
						<tr>
						
							<th><g:message code="lista.categoria.label" default="Categoria" /></th>
						
							<g:sortableColumn property="nombre" title="${message(code: 'lista.nombre.label', default: 'Nombre')}" />
						
						</tr>
					</thead>
					<tbody>
						<g:each in="${listaInstanceList}" status="i" var="listaInstance">
							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">						

								<td>${fieldValue(bean: listaInstance, field: "categoria")}</td>

								<td><g:link action="show" id="${listaInstance.id}">${fieldValue(bean: listaInstance, field: "nombre")}</g:link></td>
							
							</tr>
						</g:each>
					</tbody>
				</table>

				<!--<div class="pagination">
					<g:paginate total="${listaInstanceCount ?: 0}" />
				</div>-->
			</div>

			<!--<div id="second">When you post a new question, other users will almost immediately see it and try to provide good answers. This often happens in a 	matter of minutes, so be sure to check back frequently when your question is still new for the best response.</div>-->

		
	</body>
</html>
