
<%@ page import="gokulist.Lista" %>
<%@ page import="gokulist.Todo" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'lista.label', default: 'Lista')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-lista" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<!--<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>-->
				<!--<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>-->
				<!--<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>-->
				<li><g:link class="list" action="index"><g:message code="Ver Listas" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-lista" class="content scaffold-show" role="main">

			<div id="wrapper">
				<div class="first" >

					<h1><g:message code="Detalle" args="[entityName]" /></h1>

					<g:if test="${flash.message}">
					<div class="message" role="status">${flash.message}</div>
					</g:if>

					<ol class="property-list lista">
					
						<g:if test="${listaInstance?.categoria}">
						<li class="fieldcontain">
							<span id="categoria-label" class="property-label"><g:message code="lista.categoria.label" default="Categoria" /></span>
							
								<span class="property-value" aria-labelledby="categoria-label">${listaInstance?.categoria?.encodeAsHTML()}</span>
							
						</li>
						</g:if>
					
						<g:if test="${listaInstance?.nombre}">
						<li class="fieldcontain">
							<span id="nombre-label" class="property-label"><g:message code="lista.nombre.label" default="Nombre" /></span>
							
								<span class="property-value" aria-labelledby="nombre-label"><g:fieldValue bean="${listaInstance}" field="nombre"/></span>
							
						</li>
						</g:if>
					
						<!--<g:if test="${listaInstance?.usuarios}">
						<li class="fieldcontain">
							<span id="usuarios-label" class="property-label"><g:message code="lista.usuarios.label" default="Usuarios" /></span>
							
								<g:each in="${listaInstance.usuarios}" var="u">
								<span class="property-value" aria-labelledby="usuarios-label"><g:link controller="usuario" action="show" id="${u.id}">${u?.encodeAsHTML()}</g:link></span>
								</g:each>
							
						</li>
						</g:if>-->
					
					</ol>
				</div>
				<!--<div id="second">When you post a new question, other users will almost immediately see it and try to provide good answers. This often happens in a 	matter of minutes, so be sure to check back frequently when your question is still new for the best response.</div>-->

				<div id="list-todo" class="second" role="main">
					<h1><g:message code="Tareas" args="[entityName]" /></h1>

					<!--<g:if test="${flash.message}">
						<div class="message" role="status">${flash.message}</div>
					</g:if>-->
					<table>
					<thead>
							<tr>
							
								<g:sortableColumn property="completada" title="${message(code: 'todo.completada.label', default: 'Completada')}" />
							
								<g:sortableColumn property="contenido" title="${message(code: 'todo.contenido.label', default: 'Contenido')}" />
							
								<g:sortableColumn property="dateCreated" title="${message(code: 'todo.dateCreated.label', default: 'Date Created')}" />
							
								<!--<th><g:message code="todo.lista.label" default="Lista" /></th>-->
							
							</tr>
						</thead>
						<tbody>
						<g:each in="${todoInstanceList}" status="i" var="todoInstance">
							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
							
								<!--<td><g:link action="show" id="${todoInstance.id}">${fieldValue(bean: todoInstance, field: "completada")}</g:link></td>-->
								<g:if test="${todoInstance.completada==true}">
								     <td>SI</td>
								</g:if>
								<g:else>
								     <td>NO</td>
								</g:else>
							
								<td>${fieldValue(bean: todoInstance, field: "contenido")}</td>
							
								<td><g:formatDate date="${todoInstance.dateCreated}" /></td>
							
								<!--<td>${fieldValue(bean: todoInstance, field: "lista")}</td>-->
							
							</tr>
						</g:each>
						</tbody>
					</table>
				</div>

				<div class="third">
					<g:form url="[resource:todoInstance, action:'saveTodo']" >
						<fieldset class="form">
							<g:render template="ingreso_todo"/>
						</fieldset>
						<fieldset class="buttons">
							<g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
						</fieldset>
					</g:form>
				</div>
				
			</div>
				
			<g:form url="[resource:listaInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${listaInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>