package gokulist

public enum NivelUsuario {

 FREE("Free"),
 BRONZE("Bronze"),
 SILVER("Silver"),
 GOKU("Goku")

 private final String value

 NivelUsuario(String value){
  this.value = value;
 }

 String toString() {
  value
 }

 String getKey() {
  name()
 }

}