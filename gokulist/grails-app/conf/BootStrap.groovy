import gokulist.Usuario
import gokulist.Categoria
import gokulist.NivelUsuario

class BootStrap {

    def init = { servletContext ->

        if(Usuario.count() == 0){
        	new Usuario (nombre: 'Jose', nombreUsuario: 'pepeloco', email:'pepe@gmail.com', password:'123456', cantListas:0, nivelUsuario:NivelUsuario.FREE, fechaSuscripcion:Date.parse("yyyy-MM-dd hh:mm:ss", "2014-04-03 1:23:45")).save(flush:true, failOnError:true)
            new Usuario (nombre: 'Juan', nombreUsuario: 'juancho', email:'juan@gmail.com', password:'123456', cantListas:0, nivelUsuario:NivelUsuario.BRONZE, fechaSuscripcion:Date.parse("yyyy-MM-dd hh:mm:ss", "2014-04-03 1:23:45")).save(flush:true, failOnError:true)
            new Usuario (nombre: 'Miguel', nombreUsuario: 'miguelito', email:'migue@gmail.com', password:'123456', cantListas:0, nivelUsuario:NivelUsuario.SILVER, fechaSuscripcion:Date.parse("yyyy-MM-dd hh:mm:ss", "2014-04-03 1:23:45")).save(flush:true, failOnError:true)
            new Usuario (nombre: 'Goku', nombreUsuario: 'gokucap', email:'goku@gmail.com', password:'123456', cantListas:0, nivelUsuario:NivelUsuario.GOKU, fechaSuscripcion:Date.parse("yyyy-MM-dd hh:mm:ss", "2014-04-03 1:23:45")).save(flush:true, failOnError:true)
            new Usuario (nombre: 'Ernesto', nombreUsuario: 'semilla', email:'semilla@gmail.com', password:'123456', cantListas:0, nivelUsuario:NivelUsuario.FREE, fechaSuscripcion:Date.parse("yyyy-MM-dd hh:mm:ss", "2014-04-03 1:23:45")).save(flush:true, failOnError:true)

        	new Categoria (nombre: 'Viajes').save()
        	new Categoria (nombre: 'Compras').save()
        	new Categoria (nombre: 'Trabajo').save()
        	new Categoria (nombre: 'Entretenimiento').save()
        	new Categoria (nombre: 'Hogar').save()
            new Categoria (nombre: 'Mascotas').save()
        	new Categoria (nombre: 'Otros').save()
        }
    }
    def destroy = {
    }
}
