class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

         // protected pages
        //"/admin/$controller/$action?/$id?"()

        //"/admin/$controller/$action?/$id?"{controller = "UsuarioController"}

        "/"(view:"/index")
        "500"(view:'/error')
	}
}
