package gokulist



import spock.lang.*

/**
 *
 */
class UsuarioSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }


    def "guardar nuestro primer usuario"(){
    	given: "un nuevo usuario"
    		def usuario = new Usuario(nombre:'jose', nombreUsuario:'pepeloco', password:'123456', email:'pepe@outlook.com')
    	when: "el usuario es guardado"
    		usuario.save()
    	then: "se salvo exitosamente"
    		usuario.errors.errorCount == 0
    		usuario.id != null 
    }
}
